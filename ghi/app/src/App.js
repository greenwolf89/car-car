import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import { AppointmentList, TechnicianList, ServiceHistory, SaleHistory, SalesList, SalespersonsList, AutomobileList, ManufacturerList, VehicleModelList} from './components'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />

          <Route path="inventory/automobiles" element={<AutomobileList />} />
          <Route path="inventory/manufacturers" element={<ManufacturerList />} />
          <Route path="inventory/models" element={<VehicleModelList />} />

          <Route path="sales/history" element={<SaleHistory />} />
          <Route path="sales" element={<SalesList />} />

          <Route path="service/appointments/history" element={<ServiceHistory />} />
          <Route path="service/appointments" element={<AppointmentList />} />

          <Route path="employees/technicians" element={<TechnicianList />} />
          <Route path="employees/salespersons" element={<SalespersonsList />} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
