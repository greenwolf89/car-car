import { NavLink, Link } from 'react-router-dom';
import NavDropdown from 'react-bootstrap/NavDropdown'

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Car<sup>2</sup></NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <NavDropdown
            id="nav-dropdown"
            title="Inventory">
              <NavDropdown.Item to="inventory/automobiles" as={Link}>Automobiles</NavDropdown.Item>
              <NavDropdown.Item to="inventory/manufacturers" as={Link}>Manufacturers</NavDropdown.Item>
              <NavDropdown.Item to="inventory/models" as={Link}>Models</NavDropdown.Item>
            </NavDropdown>
          <NavDropdown
            id="nav-dropdown"
            title="Sales">
              <NavDropdown.Item to="sales" as={Link}>Sales</NavDropdown.Item>
              <NavDropdown.Item to="sales/history" as={Link}>Records</NavDropdown.Item>
            </NavDropdown>
          <NavDropdown
            id="nav-dropdown"
            title="Service">
              <NavDropdown.Item to="service/appointments" as={Link}>Current Appointments</NavDropdown.Item>
              <NavDropdown.Item to="service/appointments/history" as={Link}>Automobile Service History</NavDropdown.Item>
            </NavDropdown>
            <NavDropdown
            id="nav-dropdown"
            title="Employees">
              <NavDropdown.Item to="employees/technicians" as={Link}>Technicians</NavDropdown.Item>
              <NavDropdown.Item to="employees/salespersons" as={Link}>Salespersons</NavDropdown.Item>
            </NavDropdown>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
