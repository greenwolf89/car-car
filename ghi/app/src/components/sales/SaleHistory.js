import { React, useEffect, useState } from 'react'
import { Form } from 'react-bootstrap';

const SaleHistory = () => {
  const [sales, setSales] = useState([]);
  const [salespersons, setSalespersons] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState("0");


  const getSales = async (salesperson) => {
    let response = await fetch( "http://localhost:8090/api/sales/")
    let data = await response.json()
    setSales(data.sales)
};

useEffect(() => {
    getSales();
}, [])

const filteredSales = () => {
  if (selectedSalesperson === "0"){
    return sales
  } else {
    return sales.filter(sale => sale.salesperson.employee_number === selectedSalesperson )
  }
}


const getSalespersons = async () => {
  let response = await fetch("http://localhost:8090/api/salespersons/")
  let data = await response.json()
  setSalespersons(data.salespersons)
};

useEffect(() => {
  getSalespersons();
}, [])
return (
  <>
      <h4>Salesperson History</h4>
      <Form.Group className='mb-3'>
        <Form.Label>Salesperson</Form.Label>
        <Form.Select onChange={e => {
          console.log(e.target.value, typeof e.target.value);
          setSelectedSalesperson(e.target.value);
        } } aria-label="Salesperson" className='w-[45px]'>
          <option value={0}>Choose a salesperson</option>
          {salespersons.map(salesperson => {
            return (
              <option key={salesperson.employee_number} value={salesperson.employee_number}>
                {salesperson.name}
              </option>
            )
          })}
          </Form.Select>
      </Form.Group>
      <table className='table table-striped'>
          <thead>
              <tr>
                  <th>Salesperson</th>
                  <th>Customer Name</th>
                  <th>Automobile VIN</th>
                  <th>Price</th>
              </tr>
          </thead>
          <tbody>
              {filteredSales().map(sale => {
                  return (
                      <tr key={sale.id}>
                          <td>{sale.salesperson.name}</td>
                          <td>{sale.customer.name}</td>
                          <td>{sale.automobile.vin}</td>
                          <td>{sale.price}</td>
                      </tr>
                  )
              })}
          </tbody>
      </table>
  </>
)
}



export default SaleHistory
