import { useEffect, useState } from 'react'
import { Form, Button, Container } from 'react-bootstrap';


const SalesList = () => {
    const [sales, setSales] = useState([]);
    const [customerFormData, setCustomerFormData] = useState({
        name: "",
        address: "",
        phone_number: "",
    })
    const [automobileVOs, setAutomobileVOs] = useState([])
    const [salesFormData, setSalesFormData] = useState({
        automobile: "",
        salesperson: "",
        customer: "",
        price: "",
    })
    const [customers, setCustomers] = useState([])
    const getCustomers = async () => {
        let response = await fetch("http://localhost:8090/api/customers/")
        let data = await response.json()
        setCustomers(data.customers)
    };

    useEffect(() => {
        getCustomers();
    }, [])

    const [salespersons, setSalespersons] = useState([])
    const getSalespersons = async () => {
        let response = await fetch("http://localhost:8090/api/salespersons/")
        let data = await response.json()
        setSalespersons(data.salespersons)
    };

    useEffect(() => {
        getSalespersons();
    }, [])

    const getAutomobileVOs = async () => {
        let response = await fetch("http://localhost:8090/api/sales/automobiles/")
        let data = await response.json()
        setAutomobileVOs(data.automobiles.filter(auto => auto.sold !== true))
    }
    useEffect(() => {
        getAutomobileVOs();
    }, [])

    const getSales = async () => {
        let response = await fetch("http://localhost:8090/api/sales/")
        let data = await response.json()
        setSales(data.sales)
    };

    useEffect(() => {
        getSales();
    }, [])

    const handleSaleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...salesFormData };
        const salesrecordsUrl = "http://localhost:8090/api/sales/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const postResponse = await fetch(salesrecordsUrl, fetchConfig);
        if (postResponse.ok) {
            setSalesFormData({
                automobile: "",
                salesperson: "",
                customer: "",
                price: "",
            })
        } setAutomobileVOs([]);
        setCustomers([]);
        setSalespersons([]);
        getSales();
    }

    const handleCustomerSubmit = async (event) => {
        event.preventDefault();
        const data = { ...customerFormData };
        const customersUrl = "http://localhost:8090/api/customers/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const postResponse = await fetch(customersUrl, fetchConfig);
        if (postResponse.ok) {
            setCustomerFormData({
                name: "",
                address: "",
                phone_number: "",
            })
        } getCustomers();
    }

    const handleComplete = async (vin) => {
        await fetch(`http://localhost:8090/api/sales/automobiles/${vin}/`, {
            method: 'PUT',
            body: JSON.stringify({
                "sold": true
            }),
            headers: {
                'Content-Type': "application/json",
            }
        });
        setSales(sales);
        getSales();
    }

    const buttonVisible = 'btn btn-success'
    const buttonHidden = 'd-none'

    return (
        <>
            <div className="mx-auto" container="true" style={{
                display: 'block',
                width: 700,
                padding: 30,
                alignContent: 'center',
            }}>
                <div className='col'>
                    <div className='card shadow'>
                        <div className='card-body'>
                            <h4>Add a potential customer</h4>
                            <Form onSubmit={e => handleCustomerSubmit(e)}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control value={customerFormData.name} onChange={e => setCustomerFormData({ ...customerFormData, name: e.target.value })} type="text" placeholder="Enter the customer's first and last name" />
                                </Form.Group>
                                <Form.Group className="mb-3">
                                    <Form.Label>Address</Form.Label>
                                    <Form.Control value={customerFormData.address} onChange={e => setCustomerFormData({ ...customerFormData, address: e.target.value })} type="text" placeholder="Enter the customer's address" />
                                </Form.Group>
                                <Form.Group className="mb-3">
                                    <Form.Label>Phone Number</Form.Label>
                                    <Form.Control value={customerFormData.phone_number} onChange={e => setCustomerFormData({ ...customerFormData, phone_number: e.target.value })} type="text" placeholder="Enter the customer's phone number" />
                                </Form.Group>
                                <Container>
                                    <Button variant="primary" type="submit">
                                        Submit
                                    </Button>
                                </Container>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
            <div className="mx-auto" container="true" style={{
                display: 'block',
                width: 700,
                padding: 30,
                alignContent: 'center',
            }}>
                <div className="col">
                    <div className='card shadow'>
                        <div className='card-body'>
                            <h4>Create A Sale Record</h4>
                            <Form onSubmit={e => handleSaleSubmit(e)}>
                                <Form.Group className='mb-3'>
                                    <Form.Label>Automobile</Form.Label>
                                    <Form.Select onChange={e => setSalesFormData({ ...salesFormData, automobile: e.target.value })} aria-label="Automobile" className='w-[45px]'>
                                        <option>Choose an automobile</option>
                                        {automobileVOs.map(auto => {
                                            return (
                                                <option key={auto.vin} value={auto.vin}>
                                                    {auto.vin}
                                                </option>
                                            )
                                        })}
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className='mb-3'>
                                    <Form.Label>Saleperson</Form.Label>
                                    <Form.Select onChange={e => setSalesFormData({ ...salesFormData, salesperson: e.target.value })} aria-label="Salesperson" className='w-[45px]'>
                                        <option>Choose a salesperson</option>
                                        {salespersons.map(salesperson => {
                                            return (
                                                <option key={salesperson.employee_number} value={salesperson.name}>
                                                    {salesperson.name}
                                                </option>
                                            )
                                        })}
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className='mb-3'>
                                    <Form.Label>Customer</Form.Label>
                                    <Form.Select onChange={e => setSalesFormData({ ...salesFormData, customer: e.target.value })} aria-label="Customer">
                                        <option>Choose a customer</option>
                                        {customers.map(customer => {
                                            return (
                                                <option key={customer.name} value={customer.name}>
                                                    {customer.name}
                                                </option>
                                            )
                                        })}
                                    </Form.Select>
                                </Form.Group>
                                <Form.Group className="mb-3">
                                    <Form.Label>Price</Form.Label>
                                    <Form.Control value={salesFormData.price} onChange={e => setSalesFormData({ ...salesFormData, price: e.target.value })} type="text" placeholder="Enter your sale price" />
                                </Form.Group>
                                <Container>
                                    <Button variant="primary" type="submit">
                                        Submit
                                    </Button>
                                </Container>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
            <h1>Sales</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Employee number</th>
                        <th>Customer Name</th>
                        <th>Automobile VIN</th>
                        <th>Price</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {sales && sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.name}</td>
                                <td>{sale.salesperson.employee_number}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.price}</td>
                                <td>{sale.automobile.sold ? "Sold" : "Pending"}</td>
                                <td><button onClick={async () => handleComplete(sale.automobile.vin)} type="button" className={sale.automobile.sold ? buttonHidden : buttonVisible}>Complete Sale</button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default SalesList
