import { useEffect, useState } from 'react';
import { Form, Button, Container } from 'react-bootstrap';

const ManufacturerList = () => {
    const [manufacturers, setManufacturers] = useState([]);
    const [formData, setFormData] = useState({
        name: '',
        icon: ''
    })

    const getManufacturers = async () => {
        let response = await fetch('http://localhost:8100/api/manufacturers/')
        let data = await response.json()
        setManufacturers(data.manufacturers)
    };
    useEffect(() => {
        getManufacturers()
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...formData };
        const manufacturersUrl = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(manufacturersUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                name: '',
                icon: '',
            })
        } getManufacturers();
    };

    return (
        <>
            <div className="mx-auto" style={{
                display: 'block',
                width: 700,
                padding: 30
            }}>
                <div className='col'>
                    <div className='card shadow'>
                        <div className='card-body'>
                            <h4>Add a Manufacturer</h4>
                            <Form onSubmit={e => handleSubmit(e)}>
                                <Form.Group className='mb-3'>
                                    <Form.Label>Name:</Form.Label>
                                    <Form.Control value={formData.name}
                                        onChange={e => setFormData({ ...formData, name: e.target.value })} type="text"
                                        placeholder="Name" />
                                </Form.Group>
                                <Form.Group className='mb-3'>
                                    <Form.Label>Icon:</Form.Label>
                                    <Form.Control value={formData.icon}
                                        onChange={e => setFormData({ ...formData, icon: e.target.value })} type="text"
                                        placeholder="http://" />
                                </Form.Group>
                                <Container>
                                    <Button variant="primary" type="submit">
                                        Submit
                                    </Button>
                                </Container>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
            <h1>Manufacturers</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Icon</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers && manufacturers.map(manufacturer => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                                <td><img width={50} src={manufacturer.icon} /></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ManufacturerList
