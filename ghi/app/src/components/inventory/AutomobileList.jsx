import { useEffect, useState } from "react"
import { Form, Button, Container } from 'react-bootstrap';

const AutomobileList = () => {
  const [autos, setAutos] = useState([]);
  const [formData, setFormData] = useState({
    color: "",
    year: "",
    vin: "",
    model_id: ""

  })
  const [models, setModels] = useState([])

  const getAutos = async () => {
    let response = await fetch("http://localhost:8100/api/automobiles/")
    let data = await response.json()
    setAutos(data.autos)
  };
  useEffect(() => {
    getAutos()
  }, []);

  const getModels = async () => {
    let response = await fetch("http://localhost:8100/api/models/")
    let data = await response.json()
    setModels(data.models)
  };
  useEffect(() => {
    getModels();
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = { ...formData };
    const autosUrl = "http://localhost:8100/api/automobiles/"
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(autosUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        color: "",
        year: "",
        vin: "",
        model_id: "",
      })
    }; setModels([]);
    getModels();
    getAutos();
  };

  return (
    <>
      <div className='mx-auto' style={{
        display: "block",
        width: 700,
        padding: 30
      }}>
        <div className='col'>
          <div className='card shadow'>
            <div className='card-body'>
              <h4>Add an Automobile</h4>
              <Form onSubmit={e => handleSubmit(e)}>
                <Form.Group className='mb-3'>
                  <Form.Label>Color</Form.Label>
                  <Form.Control value={formData.name}
                    onChange={e => setFormData({ ...formData, color: e.target.value })} type="text"
                    placeholder='Red' />
                </Form.Group>
                <Form.Group className='mb-3'>
                  <Form.Label>Year</Form.Label>
                  <Form.Control value={formData.year}
                    onChange={e => setFormData({ ...formData, year: e.target.value })} type="text"
                    placeholder='20XX' />
                </Form.Group>
                <Form.Group className="mb-3">
                  <Form.Label>Vin</Form.Label>
                  <Form.Control value={formData.vin} onChange={e => setFormData({ ...formData, vin: e.target.value })} aria-label="Vin" type="text"
                  />
                </Form.Group>
                <Form.Group className="mb-3">
                  <Form.Label>Model</Form.Label>
                  <Form.Select onChange={e => setFormData({ ...formData, model_id: e.target.value })} aria-label="Model">
                    <option>Choose a model</option>
                    {models.map(model => {
                      return (
                        <option key={model.id} value={model.id}>
                          {model.name}
                        </option>
                      );
                    })}
                  </Form.Select>
                </Form.Group>
                <Container>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Container>
              </Form>
            </div>
          </div>
        </div>
      </div>
      <div style={{ alignContent: "center" }}>
        <h1>Automobiles</h1>
        <table className='table table-striped'>
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Model</th>
              <th>Color</th>
              <th>Year</th>
              <th>Vin</th>
            </tr>
          </thead>
          <tbody>
            {autos.map(auto => {
              return (
                <tr key={auto.id}>
                  <td>{auto.model.manufacturer.name}</td>
                  <td>{auto.model.name}</td>
                  <td>{auto.color}</td>
                  <td>{auto.year}</td>
                  <td>{auto.vin}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </>
  )
}

export default AutomobileList
