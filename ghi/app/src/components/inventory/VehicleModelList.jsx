import { useEffect, useState } from "react"
import { Form, Button, Container } from 'react-bootstrap';

const VehicleModelList = () => {
  const [models, setModels] = useState([]);
  const [formData, setFormData] = useState({
    name: '',
    picture_url: '',
    manufacturer_id: ''
  })

  const [manufacturers, setManufacturers] = useState([])
  const getManufacturers = async () => {
    let response = await fetch('http://localhost:8100/api/manufacturers/')
    let data = await response.json()
    setManufacturers(data.manufacturers)
  };
  useEffect(() => {
    getManufacturers();
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = { ...formData };
    const modelsUrl = 'http://localhost:8100/api/models/'
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(modelsUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        name: '',
        picture_url: '',
        manufacturer_id: ''
      })
    }; setManufacturers([]);
    getManufacturers();
    getModels();
  };

  const getModels = async () => {
    let response = await fetch('http://localhost:8100/api/models/')
    let data = await response.json()
    setModels(data.models)
  };
  useEffect(() => {
    getModels()
  }, []);

  return (
    <>
      <div className="mx-auto" style={{
        display: 'block',
        width: 700,
        padding: 30
      }}>
        <div className='col'>
          <div className='card shadow'>
            <div className='card-body'>
              <h4>Add a Model</h4>
              <Form onSubmit={e => handleSubmit(e)}>
                <Form.Group className='mb-3'>
                  <Form.Label>Name:</Form.Label>
                  <Form.Control value={formData.name}
                    onChange={e => setFormData({ ...formData, name: e.target.value })} type="text"
                    placeholder="Name" />
                </Form.Group>
                <Form.Group className='mb-3'>
                  <Form.Label>Image:</Form.Label>
                  <Form.Control value={formData.picture_url}
                    onChange={e => setFormData({ ...formData, picture_url: e.target.value })} type="text"
                    placeholder="http://" />
                </Form.Group>
                <Form.Group className='mb-3'>
                  <Form.Label>Manufacturer</Form.Label>
                  <Form.Select onChange={e => setFormData({ ...formData, manufacturer_id: e.target.value })} aria-label="Technician">
                    <option>Choose a manufacturer</option>
                    {manufacturers.map(manufacturer => {
                      return (
                        <option key={manufacturer.id} value={manufacturer.id}>
                          {manufacturer.name}
                        </option>
                      );
                    })}
                  </Form.Select>
                </Form.Group>
                <Container>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Container>
              </Form>
            </div>
          </div>
        </div>
      </div>
      <div style={{ alignContent: "center" }}>
        <h1>Models</h1>
        <table className='table table-striped'>
          <thead>
            <tr>
              <th>Model Name</th>
              <th>Manufacturer</th>
              <th>Image</th>
            </tr>
          </thead>
          <tbody>
            {models && models.map(model => {
              return (
                <tr key={model.id}>
                  <td>{model.name}</td>
                  <td>{model.manufacturer.name}</td>
                  <td><img width={200} src={model.picture_url} /></td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </>
  )
}

export default VehicleModelList
