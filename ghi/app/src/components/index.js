import AutomobileList from "./inventory/AutomobileList";
import ManufacturerList from "./inventory/ManufacturerList";
import VehicleModelList from "./inventory/VehicleModelList";

import AppointmentList from "./service/AppointmentList";
import ServiceHistory from "./service/ServiceHistory";

import SaleHistory from "./sales/SaleHistory";
import SalesList from "./sales/SalesList";

import SalespersonsList from "./employees/SalespersonsList";
import TechnicianList from "./employees/TechnicianList";

export {
    AppointmentList,
    ServiceHistory,
    TechnicianList,
    SalespersonsList,
    SaleHistory,
    SalesList,
    AutomobileList,
    ManufacturerList,
    VehicleModelList
}
