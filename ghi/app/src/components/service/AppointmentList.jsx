import { useEffect, useState } from 'react';
import { Form, Button, Container } from 'react-bootstrap';

const AppointmentList = () => {
    const [appointments, setAppointments] = useState([]);
    const [formData, setFormData] = useState({
        automobile: '',
        owner: '',
        date: '',
        time: '',
        technician: '',
        service: '',
        finished: false
    })
    const [technicians, setTechnicians] = useState([])

    const getTechnicians = async () => {
        let response = await fetch('http://localhost:8080/api/technicians/')
        let data = await response.json()
        setTechnicians(data.technicians)
    };
    useEffect(() => {
        getTechnicians();
    }, [])

    const getAppointments = async () => {
        let response = await fetch('http://localhost:8080/api/appointments/')
        let data = await response.json()
        setAppointments(data.appointments.filter(appointment => appointment.finished !== true))
    };

    useEffect(() => {
        getAppointments();
    }, [])


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...formData };
        const appointmentsUrl = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const postResponse = await fetch(appointmentsUrl, fetchConfig);
        if (postResponse.ok) {
            setFormData({
                automobile: '',
                owner: '',
                date: '',
                time: '',
                technician: '',
                service: '',
                finished: false
            })
        } setTechnicians([]);
        getTechnicians();
        getAppointments();
    }
    const check = <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-check" viewBox="0 0 16 16">
        <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z" />
    </svg>
    const redX = <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x" viewBox="0 0 16 16">
        <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
    </svg>

    const handleCancel = async (appointmentID) => {
        await fetch(`http://localhost:8080/api/appointments/${appointmentID}/`, { method: 'DELETE' })
            .then(() => setAppointments(appointments.filter(appointment => appointment.id !== appointmentID)))
    }

    const handleFinish = async (appointment, appointmentID) => {
        await fetch(`http://localhost:8080/api/appointments/${appointmentID}/`, {
            method: 'PUT',
            body: JSON.stringify({
                "automobile": appointment.automobile.vin,
                "owner": appointment.owner,
                "date": appointment.date,
                "time": appointment.time,
                "technician": appointment.technician.name,
                "service": appointment.service,
                "finished": true
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(() => setAppointments(appointments.filter(appointment => appointment.id !== appointmentID)))
    }

    return (
        <>
            <div className="mx-auto" container="true" style={{
                display: 'block',
                width: 700,
                padding: 30,
                alignContent: 'center',
            }}>
                <div className='col'>
                    <div className='card shadow'>
                        <div className='card-body'>
                            <h4>Set up a Service Appointment</h4>
                            <Form onSubmit={e => handleSubmit(e)}>
                                <Form.Group className='mb-3'>
                                    <Form.Label>Automobile VIN:</Form.Label>
                                    <Form.Control value={formData.automobile} onChange={e => setFormData({ ...formData, automobile: e.target.value })} type="text"
                                        placeholder="VIN#" />
                                </Form.Group>
                                <Form.Group className='mb-3'>
                                    <Form.Label>Owner Name</Form.Label>
                                    <Form.Control value={formData.owner} onChange={e => setFormData({ ...formData, owner: e.target.value })} type="text"
                                        placeholder="Owner" />
                                </Form.Group>
                                <Form.Group className='mb-3'>
                                    <Form.Label>Date</Form.Label>
                                    <Form.Control value={formData.date} onChange={e => setFormData({ ...formData, date: e.target.value })} type="date" placeholder="" />
                                </Form.Group>
                                <Form.Group className='mb-3'>
                                    <Form.Label>Time</Form.Label>
                                    <Form.Control value={formData.time} onChange={e => setFormData({ ...formData, time: e.target.value })} type="time" placeholder="" />
                                </Form.Group>
                                <Form.Group className='mb-3'>
                                    <Form.Label>Reason for Service</Form.Label>
                                    <Form.Control value={formData.service} onChange={e => setFormData({ ...formData, service: e.target.value })} type="text" placeholder="Reason" />
                                </Form.Group>
                                <Form.Group className='mb-3'>
                                    <Form.Label>Technician</Form.Label>
                                    <Form.Select onChange={e => setFormData({ ...formData, technician: e.target.value })} aria-label="Technician" className='w-[45px]'>
                                        <option>Choose a technician</option>
                                        {technicians.map(technician => {
                                            return (
                                                <option key={technician.employee_number} value={technician.name}>
                                                    {technician.name}
                                                </option>
                                            );
                                        })}
                                    </Form.Select>
                                </Form.Group>
                                <Container>
                                    <Button className='mb-3' variant="primary" type="submit" block>
                                        Submit
                                    </Button>
                                </Container>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
            <h1>Service Appointments</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>VIP</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments && appointments.map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.automobile.vin}</td>
                                <td>{appointment.owner}</td>
                                <td>{appointment.automobile.vip ? check : redX}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.technician.name}</td>
                                <td>{appointment.service}</td>
                                <td><button onClick={async () => handleCancel(appointment.id)} type="button" className="btn btn-danger">Cancel</button></td>
                                <td><button onClick={async () => handleFinish(appointment, appointment.id)} type="button" className="btn btn-success">Finish</button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>

    )
}

export default AppointmentList
