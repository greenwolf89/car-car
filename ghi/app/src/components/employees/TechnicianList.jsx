import { useEffect, useState } from 'react';
import { Form, Button, Container } from 'react-bootstrap';

const TechnicianList = () => {
  const [technicians, setTechnicians] = useState([]);
  const [formData, setFormData] = useState({
    name: '',
    employee_number: '',
  })

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = { ...formData };
    const techniciansUrl = 'http://localhost:8080/api/technicians/'
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(techniciansUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        name: '',
        employee_number: '',
      })
    } getTechnicians();
  };

  const getTechnicians = async () => {
    let response = await fetch('http://localhost:8080/api/technicians/')
    let data = await response.json()
    setTechnicians(data.technicians)
  }

  useEffect(() => {
    getTechnicians();
  }, [])
  return (
    <>
      <div className="mx-auto" style={{
        display: 'block',
        width: 700,
        padding: 30
      }}>
        <div className='col'>
          <div className='card shadow'>
            <div className='card-body'>
              <h4>Add a Technician</h4>
              <Form onSubmit={e => handleSubmit(e)}>
                <Form.Group className='mb-3'>
                  <Form.Label>Name:</Form.Label>
                  <Form.Control value={formData.name}
                    onChange={e => setFormData({ ...formData, name: e.target.value })} type="text"
                    placeholder="Name" />
                </Form.Group>
                <Form.Group className='mb-3'>
                  <Form.Label>Employee Number:</Form.Label>
                  <Form.Control value={formData.employee_number}
                    onChange={e => setFormData({ ...formData, employee_number: e.target.value })} type="integer"
                    placeholder="#" />
                </Form.Group>
                <Container>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Container>
              </Form>
            </div>
          </div>
        </div>
      </div>
      <h1>CarCar Technicians</h1>
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {technicians && technicians.map(technician => {
            return (
              <tr key={technician.id}>
                <td>{technician.name}</td>
                <td>{technician.employee_number}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </>
  )
}

export default TechnicianList
