import { useEffect, useState } from 'react'
import { Button, Container, Form } from 'react-bootstrap'

const SalespersonsList = () => {
    const [salespersons, setSalespersons] = useState([]);
    const [formData, setFormData] = useState({
        name: "",
        employee_number: "",
    })

    const getSalespersons = async () => {
        let response = await fetch('http://localhost:8090/api/salespersons/')
        let data = await response.json();
        setSalespersons(data.salespersons);
    }

    useEffect(() => {
        getSalespersons();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...formData };
        const salespersonsUrl = "http://localhost:8090/api/salespersons/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const postResponse = await fetch(salespersonsUrl, fetchConfig);
        if (postResponse.ok) {
            setFormData({
                name: "",
                employee_number: "",
            })
        } getSalespersons();
    }

    return (
        <>
            <div className="mx-auto" container="true" style={{
                display: 'block',
                width: 700,
                padding: 30,
                alignContent: 'center',
            }}>
                <div className='col'>
                    <div className='card shadow'>
                        <div className='card-body'>
                            <h4>Register a Salesperson</h4>
                            <Form onSubmit={e => handleSubmit(e)}>
                                <Form.Group className="mb-3">
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control value={formData.name} onChange={e => setFormData({ ...formData, name: e.target.value })} type="text" placeholder="Name" />
                                </Form.Group>
                                <Form.Group className="mb-3">
                                    <Form.Label>Employee number</Form.Label>
                                    <Form.Control value={formData.employee_number} onChange={e => setFormData({ ...formData, employee_number: e.target.value })} type="text" placeholder="#" />
                                </Form.Group>
                                <Container>
                                    <Button variant="primary" type="submit">
                                        Submit
                                    </Button>
                                </Container>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
            <h1>Salespersons</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {salespersons && salespersons.map(salesperson => {
                        return (
                            <tr key={salesperson.id}>
                                <td>{salesperson.name}</td>
                                <td>{salesperson.employee_number}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default SalespersonsList
