# CarCar

Team:

* Jazz - Sales
* Waylen - Service

## Design
Backend:
<img src="https://i.ibb.co/gv2nFKC/Screen-Shot-2022-10-28-at-1-29-52-PM.png" alt="Screen-Shot-2022-10-28-at-1-29-52-PM" border="0">
Frontend:
<img src="https://i.ibb.co/RPKmkTv/Screen-Shot-2022-10-28-at-1-44-10-PM.png" alt="Screen-Shot-2022-10-28-at-1-44-10-PM" border="0">
## Service microservice
For Service, I created three models: Appointment, Technician, and AutomobileVO.

The Appointment Model acts as the aggregate root and contains the following properties:
automobile - references the AutomobileVO model as a foreign key;
owner - name of the automobile's owner;
date - the date for the service;
time - the time for the service;
technician - references the Technician model as a foreign key;
finished - a boolean field that defaults to False, and will be changed to True on the front end once the appointment is completed.

The AutomobileVO Model is a value object that stores the vins of the automobiles from the Inventory provider.
I set up a poller inside the Service microservice to poll for automobile data from the Automobile model in Inventory.
I then added a 'vip' property to AutomobileVO that keeps track of whether the automobile instance being selected for a service appointment matches an automobile in inventory,
hence indicating a return customer from sales to service. The boolean registers as either False (not a match) or True (a return customer) and my front end renders check or x in the Vip
column accordingly.

The Technician model contains the following properties:
name: the name of the employee;
employee_number: their unique employee id
## Sales microservice

The sales microservice consists of the AutomobileVO model that contains a character field to hold a vin number as well as a boolean to check whether the specific automobile has been sold. The Customer model which holds the necessary data for customers to be added to the database and communicate with the inventory microservice. Same for the salesperson model. The Sale model will communicate with automobilevo, the salesperson model, and the customer model in order to gather data necessary to log a pending sale to the database. The sale record form will finalize that sale and remove it from being shown in the inventory list.
