from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment

class AutomobileVoEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vip", "vin", "model"]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["id", "name", "employee_number"]


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "automobile",
        "owner",
        "date",
        "time",
        "technician",
        "service",
        "finished"
    ]

    encoders = {
        "automobile": AutomobileVoEncoder(),
        "technician": TechnicianEncoder(),
    }

