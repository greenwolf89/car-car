from django.db import models
from django.urls import reverse

# Create your models here.


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    vip = models.BooleanField(default=False)
    model=models.CharField(max_length=500)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    name = models.CharField(max_length=200, unique=True)
    employee_number = models.PositiveSmallIntegerField()

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

    class Meta:
        ordering = ("employee_number", "name")


class Appointment(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO, related_name="appointments", on_delete=models.CASCADE)
    owner = models.CharField(max_length=200)
    date = models.DateField()
    time = models.TimeField()
    technician = models.ForeignKey(
        Technician, related_name="appointments", on_delete=models.PROTECT)
    service = models.CharField(max_length=200)
    finished = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})

    class Meta:
        ordering = ("date", "time", "technician", "owner",
                    "automobile", "service", "finished")
