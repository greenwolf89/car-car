from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import AutomobileVO, Technician, Appointment
from .encoders import (TechnicianEncoder, AppointmentDetailEncoder)

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)

        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentDetailEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            vin = AutomobileVO.objects.create(vin=content["automobile"])
            content["automobile"] = vin

        try:
            technician_name = content["technician"]
            technician = Technician.objects.get(name=technician_name)
            content["technician"] = technician
        except:
            return JsonResponse({"Message": "Invalid employee ID"})

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Invalid Appointment ID"}, status=400)
    if request.method == "GET":
        return JsonResponse(
            appointment, encoder=AppointmentDetailEncoder, safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)

        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            vin = AutomobileVO.objects.create(vin=content["automobile"])
            content["automobile"] = vin

        try:
            technician_name = content["technician"]
            technician = Technician.objects.get(name=technician_name)
            content["technician"] = technician
        except:
            return JsonResponse({"Message": "Invalid employee ID"})

        Appointment.objects.filter(id=pk).update(**content)

        appointment = Appointment.objects.get(id=pk)

        return JsonResponse(
            appointment, encoder=AppointmentDetailEncoder, safe=False
        )
    else:
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
