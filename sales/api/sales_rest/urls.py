from django.urls import path

from .views import api_list_sales, api_list_customers, api_list_salepersons, api_sale_detail, api_list_autovos, api_update_autovo

urlpatterns = [
    path("sales/", api_list_sales, name="api_create_sale"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("salespersons/", api_list_salepersons, name="api_list_salespersons"),
    path("sales/<int:pk>/",api_sale_detail, name="api_sale_detail"),
    path("sales/automobiles/", api_list_autovos, name="api_list_autovos"),
    path("sales/automobiles/<str:vin>/", api_update_autovo, name="api_update_autovos")
]
