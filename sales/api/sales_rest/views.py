from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import AutomobileVO, Customer, Salesperson, Sale

from .encoders import AutomobileVOEncoder, SaleEncoder, SalespersonEncoder, CustomerEncoder

@require_http_methods(["GET"])
def api_list_autovos(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder,
        )

@require_http_methods(["PUT"])
def api_update_autovo(request, vin):
    try:
        content = json.loads(request.body)
        auto = AutomobileVO.objects.get(vin=vin)

        props = ["sold"]
        for prop in props:
            if prop in content:
                    setattr(auto, prop, content[prop])
        auto.save()
        return JsonResponse(
            auto,
            encoder=AutomobileVOEncoder,
            safe=False,
            )
    except AutomobileVO.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["GET", "POST"])
def api_list_salepersons(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)

        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False
        )

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)

        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )




@require_http_methods(["GET", "DELETE"])
def api_sale_detail(request, pk):
    try:
        sale = Sale.objects.get(id=pk)
    except Sale.DoesNotExist:
        return JsonResponse({"message": "Invalid Sale ID"}, status=400)
    if request.method == "GET":
        return JsonResponse(
            sale, encoder=SaleEncoder, safe=False)
    else:
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})



@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            salesperson_name = content["salesperson"]
            salesperson = Salesperson.objects.get(name=salesperson_name)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse({"Message": "Invalid sales person"})

        try:
            customer_name = content["customer"]
            customer = Customer.objects.get(name=customer_name)
            content["customer"] = customer
        except:
            return JsonResponse({"Message": "Invalid customer name"})

        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            vin = AutomobileVO.objects.create(vin=content["automobile"])
            content["automobile"] = vin

        sale = Sale.objects.create(**content)
        return JsonResponse(sale, encoder=SaleEncoder, safe=False)
